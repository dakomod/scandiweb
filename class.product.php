<?php

abstract class Product {
	
	private $sku;
	private $name;
	private $price;
	private $type;
	
	public function setSKU($sku) {
		$this->sku = $sku;
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function setPrice($price) {
		$this->price = $price;
	}
	
	public function setType($type) {
		$this->type = $type;
	}
	
	public function getSKU() {
		return $this->sku;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function getPrice() {
		return $this->price;
	}
	
	public function getType() {
		return $this->type;
	}
	
	abstract public function insertData();
	abstract public function deleteData();	

}