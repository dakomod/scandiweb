<?php

require_once("class.product.php");
require_once("class.DBRepository.php");
require_once("db.php");

$repo = new DBRepository();

class insert_delete extends Product {
	
	public function insertData(){
		
		$repo = $GLOBALS['repo'];

		$this->setSKU($_POST['sku']);
		$this->setName($_POST['name']);
		$this->setPrice($_POST['price']);
		$this->setType($_POST['type']);

		$sku = $this->getSKU();
		$name = $this->getName();
		$price = $this->getPrice();
		$typeSwitcher = $this->getType();

		$table = 'products';
		$rows = 'sku';	
		$where = 'sku='.$sku;
					
		$added = false;

		$table = "products";
		$values = array($sku,$name,$price);
		$rows = "sku, name, price";	
		$product_inserted_id = json_decode($repo->insert($table, $values, $rows), true);
		
		if($product_inserted_id != ''){
			$attr_id = $_POST["attr_id"];
			$attr_value = $_POST["attr_value"];

			for($i = 0; $i < sizeof($attr_id); $i++) {
				$table = "additional_info";
				$values = array($attr_value[$i], $attr_id[$i], $product_inserted_id);
				$rows = "value, attribute_id, product_id";	
				$inserted_id = json_decode($repo->insert($table, $values, $rows), true);
		
				if($inserted_id != "") {
					$added = true;
				}
			}
				
			if($added) {
				echo "<div id='message'>Data added successfully</div>";
			} else {
				echo "<div id='message'>error in adding data</div>";
			}
		}
	}
	public function deleteData() {
		$repo = $GLOBALS["repo"];

		$number_of_deleted = 0;
		$idArray = $_POST["id_array"];
		for($i = 0; $i < sizeof($idArray); $i++) {	
			$deleted = false;		
			$table = "products";
			$where = "id=" . $idArray[$i];
			$product_delete = json_decode($repo->delete($table, $where), true);
			
			if($product_delete){
				$table = 'additional_info';
				$where = 'product_id='.$idArray[$i];
				$deleted = json_decode($repo->delete($table, $where), true);
				$number_of_deleted = $number_of_deleted + 1;
			}
		}

		if($deleted) {
			echo "<div id='message'>".$number_of_deleted . " Products deleted succcessfully</div>";
		}else{
			echo "<div id='message'>error in deleting data</div>";
		}
	}
	
}
?>