<?php

class DBRepository {

	public function tableExists($table)
    {
		$mysql = $GLOBALS["mysql"];
		$query = 'SHOW TABLES FROM scandiweb_database LIKE "' . $table . '" ';
		$stmt = $mysql->prepare($query);
		$stmt->execute(); 
		$stmt->store_result();
        if($stmt)
        {
            if($stmt->num_rows == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
		$stmt->close();
	}
	
	public function select($table, $rows = '*', $where = null, $order = null) {
		$mysql = $GLOBALS["mysql"];
		
        $query = 'SELECT ' . $rows . ' FROM ' . $table;
        if($where != null)
            $query .= ' WHERE ' . $where;
        if($order != null)
			$query .= ' ORDER BY ' . $order;
			if($this->tableExists($table))
			{
				$select = $mysql->prepare($query);
				$select->execute(); 
				$result = $select->get_result();
                $array;
				while ($row = $result->fetch_array()){
					$array[] = $row;
				}				 
				 return json_encode($array);
				 $select->close();
			 }
	 else
	 return false;
   }
		
   public function insert($table, $values, $rows = null)
    {
        if($this->tableExists($table))
        {            
            $mysql = $GLOBALS["mysql"];
            
            $query = 'INSERT INTO ' . $table;
            if($rows != null)
            {
                $query .= ' ('.$rows.')';
            }
            for($i = 0; $i < count($values); $i++)
            {
                if(is_string($values[$i]))
                    $values[$i] = '"'.$values[$i].'"';
            }
            $values = implode(',',$values);
            $query .= ' VALUES ('.$values.')';

			  $insert = $mysql->prepare($query);
              $insert->execute(); 


            if($insert) {
				return json_encode($mysql->insert_id);
            }
            else {
				return json_encode($mysql->error);
            }
            
				 $insert->close();
        }
	}
	
	
	public function delete($table,$where) {

        if($this->tableExists($table)) {            
            $mysql = $GLOBALS['mysql'];
            
            $query = 'DELETE FROM ' . $table . ' WHERE ' . $where;

            $delete = $mysql->prepare($query);
            $delete->execute(); 

            if($delete) {
                return true;
            }
            else {
				return json_encode($mysql->error);
            }
            
            $delete->close();
            
        }
	}
}