<?php
require_once("class.handler.php");

$handler = new Handler();
$getAll = json_decode($handler->getAll(), true);

?>
<div class="container-fluid">
	    	<form method="post" action="?pg=product_add">
			    <input type="submit" class="btn btn-success" style="margin-left: 1em; float: left;" value="Product Add"/>
			</form>
			    <input type="button" name="del" class="btn btn-danger" id="delete" style="margin-left: 1em; float:left;" value="Product Delete"/>
			<h1 class="text-right">Product list</h1>

			<div class="row">
				<?php  for($p=0; $p < sizeof($getAll["id"]); $p++) { ?>
							<div class="col-md-2 col-sm-4 list_card">
								<div class="card" style="margin-top: 1em; border: 2px dotted grey;">
									<div class="card-body">
										<input type="checkbox" name="main_id" value="<?=$getAll['id'][$p]; ?>">
										<input type="hidden" name="type_id" value="<?=$getAll['type'][$p]; ?>">
										<h5 class="card-title text-center" ><?=$getAll['sku'][$p]; ?></h5>
										<h6 class="card-subtitle mb-2 text-muted text-center" >Type: <?=$getAll['name'][$p]; ?></h6>
										<h6 class="card-subtitle mb-2 text-muted text-center" >Price: $<?=$getAll['price'][$p]; ?></h6>
										<h6 class="card-subtitle mb-2 text-muted text-center"><?=$getAll['dim_view'][$p].': '.$getAll['value'][$p].' '.$getAll['dim'][$p]; ?></h6>
									</div>
								</div>
							</div>
				<?php } ?>
			</div>
		</div>