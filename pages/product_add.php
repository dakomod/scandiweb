<?php
	require_once("class.handler.php");
	require_once("class.insert_delete.php");

	$handler = new Handler();
	$json = json_decode($handler->getTypes(), true);

	 
	$handler = new insert_delete();
?>

<div class="container-fluid">
		<form method="POST" action="?">
		  <input type="submit" class="btn btn-default" style="margin-left: 1em; float: left;" value="Product List"/>
		</form>
		<input type="submit" name="submit" class="btn btn-success" id="add" style="margin-left: 1em;float: left;" value="Submit"/>
		<h1 class="text-right">Product Add</h1>

	<form id="form_add">
		<div class="row">
					<div class="form-group col-md-3">
						<label for="sku_id">SKU</label>
						<input type="text" class="form-control" name="sku" id="sku_id" placeholder="Enter SKU" autofocus>
					</div>
					<div class="form-group col-md-3">
						<label for="name_id">Product Name</label>
						<input type="text" class="form-control" id="name_id" name="name" placeholder="Enter Product Name" disabled>
					</div>
					<div class="form-group col-md-3">
						<label for="price_id">Price</label>
						<input type="text" class="form-control" id="price_id" onkeypress="return numbersAndDot(event)" name="price" placeholder="Enter Price" disabled>
						<small class="form-text text-muted">Only numbers and dot allowed</small>
					</div>
					<div class="form-group col-md-3" id="select">
						<label for="exampleFormControlSelect1">Type</label>
						<select class="form-control" id="switcher" name="type" disabled>
						  <option readonly>choose type</option>
						  <?php
							for($i = 0; $i < sizeof($json); $i++) {?>
								<option value='<?php echo json_encode($json[$i]["attributes"]);?>'><?php echo $json[$i]["type"];?></option>
						  <?php	} ?>
						</select>
					</div>
		</div>
		<div class="dynamicdiv">
			<div class="row" id="dynamicInput"></div>
		</div>
	</form>
</div>