<?php
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	require_once("db.php");
?>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Scandiweb</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="index.js"></script>
		<style>
			.card-body:hover {
				background: lightgreen;
			}
		</style>
	</head>

	<body>
		<?php 
			if(isset($_GET["pg"]) && is_file("pages/" . $_GET["pg"] . ".php")) {
			 	require_once("pages/".$_GET['pg'].".php");
			}
			else {
			 	require_once("pages/product_list.php");
			}
		?>
	</body>
</html>