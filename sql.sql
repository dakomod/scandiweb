-- --------------------------------------------------------
-- Host:                         gogo-host.net
-- Server version:               10.0.38-MariaDB-cll-lve - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             10.3.0.5794
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for scandiweb_database
CREATE DATABASE IF NOT EXISTS `scandiweb_database` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `scandiweb_database`;

-- Dumping structure for table scandiweb_database.additional_info
CREATE TABLE IF NOT EXISTS `additional_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` text,
  `attribute_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

-- Dumping data for table scandiweb_database.additional_info: ~15 rows (approximately)
/*!40000 ALTER TABLE `additional_info` DISABLE KEYS */;
INSERT INTO `additional_info` (`id`, `value`, `attribute_id`, `product_id`) VALUES
	(136, '1', 2, 121),
	(137, '12', 3, 122),
	(138, '13', 4, 122),
	(139, '11', 5, 122),
	(140, '700', 1, 123),
	(141, '4000', 1, 124),
	(142, '3', 2, 125),
	(143, '43', 3, 126),
	(144, '23', 4, 126),
	(145, '9', 5, 126),
	(146, '0.5', 2, 127),
	(147, '64', 1, 128),
	(148, '77', 3, 129),
	(149, '56', 4, 129),
	(150, '44', 5, 129),
	(151, '7', 2, 130),
	(152, '5600', 1, 131);
/*!40000 ALTER TABLE `additional_info` ENABLE KEYS */;

-- Dumping structure for table scandiweb_database.attributes
CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `dimension_view` text,
  `dimension` text,
  `type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table scandiweb_database.attributes: ~5 rows (approximately)
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
INSERT INTO `attributes` (`id`, `name`, `dimension_view`, `dimension`, `type_id`) VALUES
	(1, 'size', 'Size', 'MB', 1),
	(2, 'weight', 'Weight', 'KG', 2),
	(3, 'height', 'Height', 'CM', 3),
	(4, 'width', 'Width', 'CM', 3),
	(5, 'length', 'Length', 'CM', 3);
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;

-- Dumping structure for view scandiweb_database.productlist
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `productlist` (
	`id` INT(11) NOT NULL,
	`sku` TEXT NULL COLLATE 'utf8_general_ci',
	`name` TEXT NULL COLLATE 'utf8_general_ci',
	`price` TEXT NULL COLLATE 'utf8_general_ci',
	`value` TEXT NULL COLLATE 'utf8_general_ci',
	`dimension_view` TEXT NULL COLLATE 'utf8_general_ci',
	`dimension` TEXT NULL COLLATE 'utf8_general_ci',
	`type_id` INT(11) NULL
) ENGINE=MyISAM;

-- Dumping structure for table scandiweb_database.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` text,
  `name` text,
  `price` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;

-- Dumping data for table scandiweb_database.products: ~12 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `sku`, `name`, `price`) VALUES
	(121, '111', 'Book 224', '49.99'),
	(122, '987', 'Furniture 2341', '12.45'),
	(123, '124', 'DVD 132', '9.99'),
	(124, '86', 'DVD 221', '19.92'),
	(125, '125', 'Book 909', '909'),
	(126, '243', 'Furniture', '23.99'),
	(127, '43', 'Book 22', '22'),
	(128, '867', 'DVD 64', '64'),
	(129, '153', 'Furniture', '98'),
	(130, '53', 'Book 7', '7'),
	(131, '56', 'DVD 56', '56');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table scandiweb_database.types
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table scandiweb_database.types: ~3 rows (approximately)
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` (`id`, `type`) VALUES
	(1, 'DVD-disk'),
	(2, 'Book'),
	(3, 'Furniture');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;

-- Dumping structure for view scandiweb_database.productlist
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `productlist`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `productlist` AS select `p`.`id` AS `id`,`p`.`sku` AS `sku`,`p`.`name` AS `name`,`p`.`price` AS `price`,group_concat(`ai`.`value` separator ' x ') AS `value`,`a`.`dimension_view` AS `dimension_view`,`a`.`dimension` AS `dimension`,`a`.`type_id` AS `type_id` from ((`products` `p` join `additional_info` `ai` on((`p`.`id` = `ai`.`product_id`))) join `attributes` `a` on((`ai`.`attribute_id` = `a`.`id`))) group by `ai`.`product_id`;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

ALTER TABLE `products`
  ADD UNIQUE KEY `sku` (`sku`) USING HASH;