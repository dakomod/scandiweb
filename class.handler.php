<?php
require_once("class.DBRepository.php");
require_once("db.php");

$repo = new DBRepository();

class Handler {

	function getTypes() {
		$repo = $GLOBALS["repo"];

		$table = "types";
		$rows = "id, type";		

		$get_type = json_decode($repo->select($table, $rows, "", ""), true);

		for($i=0; $i < sizeof($get_type); $i++){
			$art_id_array;
			$art_name_array;
			$art_dimension_view_array;
			$atr_dimension_array;
			
			$table = "attributes";
			$rows = "*";	
			$where = "type_id=" . $get_type[$i]['id'];
						
			$get_attributes = json_decode($repo->select($table, $rows, $where, ""), true);

			for($a=0; $a < sizeof($get_attributes); $a++) {
				$art_id_array[] = $get_attributes[$a]["id"];
				$art_name_array[] = $get_attributes[$a]["name"];
				$art_dimension_view_array[] = $get_attributes[$a]["dimension_view"];
				$atr_dimension_array[] = $get_attributes[$a]["dimension"];
			}

			$artArray = array("id"=>$art_id_array, "name"=>$art_name_array, "dimension_view"=>$art_dimension_view_array, "dimension"=>$atr_dimension_array);
						
			$ar[] = array("id"=>$get_type[$i]["id"], "type"=>$get_type[$i]["type"], "attributes"=>$artArray);
		}
		return json_encode($ar);
	}

	function getAll() {
		
		$repo = $GLOBALS["repo"];

		$table = "productlist";
		$rows = "*";	
					
		$get_all_products = json_decode($repo->select($table, $rows, "", ""), true);
		
		for($a=0; $a < sizeof($get_all_products); $a++){
			$listIdArray[] = $get_all_products[$a]["id"];
			$listSKUArray[] = $get_all_products[$a]["sku"];
			$listNameArray[] = $get_all_products[$a]["name"];
			$listPriceArray[] = $get_all_products[$a]["price"];
			$listValueArray[] = $get_all_products[$a]["value"];
			$listDimensionViewArray[] = $get_all_products[$a]["dimension_view"];
			$listDimensionArray[] = $get_all_products[$a]["dimension"];
			$listTypeIdArray[] = $get_all_products[$a]["type_id"];
		}

		$ar = array("id"=>$listIdArray, "sku"=>$listSKUArray, "name"=>$listNameArray, "price"=>$listPriceArray, "value"=>$listValueArray, "dim_view"=>$listDimensionViewArray, "dim"=>$listDimensionArray, "type"=>$listTypeIdArray);
		return json_encode($ar);
	}


	function checkSKU(){
		$repo = $GLOBALS["repo"];

		$table = "products";
		$rows = "sku";	
		$where = "sku=" . $_POST["check_sku"];
					
		$get_sku = json_decode($repo->select($table, $rows, $where, ""), true);

		if($get_sku != "") {
			echo "<div id='message'>true</div>";
		} else {
			echo "<div id='message'>false</div>";
		}
		
	}

}

?>