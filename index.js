$(document).ready(function() {
		
	$(document).on("click", "#delete", function() {
		
		var idArray = [];
		var type = []
		$("input[name=main_id]:checked").each(function() {
			var id = $(this).val();
			idArray.push(id);
			type.push($(this).siblings("input[name=type_id]").val());
		});
		
		if(idArray.length > 0) {
			var r = confirm("Are you sure?");
			if (r == true) {
				$.ajax({
					type: "POST",
					url: "helper.php",
					cache: false,
					data: {"delete": 1, "id_array": idArray, "action": "deleteData", "type": type},
					success: function(data) {
						var alertData = $(data).closest("div").first().filter("#message").text();
						alert(alertData);
						refreshList();
					},error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr);
						console.log(ajaxOptions);
						console.log(thrownError);
					  }
				});
			}
			
		} else {
			alert("choose product");
		}
	});
	
	$(document).on("change", "#sku_id", function(){
		var skuValue = this.value;
		
		checkSKU(skuValue, function(response){
			
			if(response == "true") {
				alert("Such SKU already exists, please add unique SKU");
				$('#switcher').attr("disabled", true);
				$('#name_id').attr("disabled", true);
				$('#price_id').attr("disabled", true);
			}else {
				$('#switcher').attr("disabled", false);
				$('#name_id').attr("disabled", false);
				$('#price_id').attr("disabled", false);
			}
		});
	});
	
	$(document).on("change", "#switcher", function() {
		var type = JSON.parse(this.value);
		var inputs = "";
		var info = "Only numbers and dot allowed";
		$("#dynamicInput").children().remove();

		for (var i = 0; i < type.id.length; i++) {
			inputs = inputs + '<div class="form-group col-md-3">\
							<label>' + type.dimension_view[i] +' (' + type.dimension[i] + ')</label>\
							<input type="text" onkeypress="return numbersAndDot(event)" class="form-control" name="attr_value[]" placeholder="Please enter ' + type.dimension_view[i] +' in ' + type.dimension[i] + '">\
							<small class="form-text text-muted">'+ info +'</small>\
							<input type="hidden" name="attr_id[]" value="'+type.id[i]+'">\
						  </div>';	
		}

		$("#dynamicInput").append(inputs);
		validation();
	});
	
	$("#add").on("click", function() {
		$("#form_add").submit();
	});
	$("#form_add").on("submit", function(e) {
		e.preventDefault();
		var fd = new FormData(this);
		fd.append("action", "insertData");
		
		$.ajax({
			type: "POST",
			url: "helper.php",
			cache: false,
			data: fd,
			contentType: false,
			processData: false,
			success: function(data) {
				var ret_data = $(data).filter("#message").text();
				if(ret_data == "") ret_data = "please insert SKU";
				alert(ret_data);
				$("input[name=sku]").val("");
				$("input[name=name]").val("");
				$("input[name=price]").val("");
				$("#dynamicInput").children().remove();
				$("select[name=type]").val("");
				$("#add").prop({ disabled: true});
				refreshList();
			},error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr);
				console.log(ajaxOptions);
				console.log(thrownError);
			  }
		});
	});
});


function numbersAndDot(evt) {
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46 && charCode != 44) {
        return false;
    }
    return true;
}


function validation() {
	let inputs = document.querySelectorAll('[type="text"], select');

	for (var i = 0; i < inputs.length; i++) {
	  inputs[i].addEventListener('input',() => {
		let values = [];
		inputs.forEach(v => values.push(v.value));
		add.disabled = values.includes('');
	  });
	}
}

function checkSKU(sku, callback) {
	var skuFlag = "";
	$.ajax({
		type: "POST",
		url: "helper.php",
		cache: false,
		data: {"check_sku":sku, "action":"checkSKU", "type":0},
		success: function(data) {
			skuFlag = $(data).filter("#message").text();
			callback(skuFlag);
		}
	});
}


function numberFormat(number) {
	return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
}


function refreshList() { 
	window.location.reload();
}